#ifndef PIECES_H
#define PIECES_H

/*************************************************************************************
* Includes
*************************************************************************************/
#include <ctype.h>
#include <stdint.h>

/*************************************************************************************
* Public macros
*************************************************************************************/
/* Pins definitions */
#define PIN_ROT_SIZE        4

/* Matrix definitions */
#define MATRIX_ROW_SIZE     7
#define MATRIX_COL_SIZE     7

/*************************************************************************************
* Public type definitions
*************************************************************************************/
/* Shapes */
typedef enum {
    BORDER      = 0,
    HOLE__,
    PIN___,
} shapes_t;

typedef enum {
    UP         = 0,
    RIGHT,
    DOWN,
    LEFT,
} rotations_t;

/*************************************************************************************
* External variables
*************************************************************************************/
extern const uint8_t referenceMatrix[MATRIX_ROW_SIZE][MATRIX_COL_SIZE];

#endif // PIECES_H
