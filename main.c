/*************************************************************************************
* Includes
*************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include <conio.h>
#include <time.h>
#include "pins.h"
#include "Crc32.h"

/*************************************************************************************
* Private macros
*************************************************************************************/
#define NUMBER_OF_PIECES    32
#define NUMBER_OF_REMAINING 1
#define MALLOC_ARRAY_SIZE   100
//#define USE_ROTATE_SYMETRY
//#define DEBUG_CLOCK
//#define DEBUG_PRINTF

#ifdef DEBUG_CLOCK
    /* Cálculo de ciclos de clock */
    #ifdef __i386
    __inline__ uint64_t rdtsc() {
      uint64_t x;
      __asm__ volatile ("rdtsc" : "=A" (x));
      return x;
    }
    #elif __amd64
    __inline__ uint64_t rdtsc() {
      uint64_t a, d;
      __asm__ volatile ("rdtsc" : "=a" (a), "=d" (d));
      return (d<<32) | a;
    }
    #endif
#endif

/*************************************************************************************
* Private typedefs
*************************************************************************************/
typedef struct piece_info_tag {
    int         rot;
    int         row;
    int         col;
    uint8_t     matrix[MATRIX_ROW_SIZE][MATRIX_COL_SIZE];
    uint32_t*   hashes;
    int         hashesSize;
} piece_info_t;

typedef struct statistics_tag {
    unsigned long int interractions;
    unsigned long int inserts;
    unsigned long int removes;
    unsigned long int memory;
} statistics_t;

/*************************************************************************************
* Private variables
*************************************************************************************/
/* Matrix para as peças inseridas */
uint8_t matrix[MATRIX_ROW_SIZE][MATRIX_COL_SIZE] = {
    { BORDER, BORDER, PIN___, PIN___, PIN___, BORDER, BORDER },
    { BORDER, BORDER, PIN___, PIN___, PIN___, BORDER, BORDER },
    { PIN___, PIN___, PIN___, PIN___, PIN___, PIN___, PIN___ },
    { PIN___, PIN___, PIN___, HOLE__, PIN___, PIN___, PIN___ },
    { PIN___, PIN___, PIN___, PIN___, PIN___, PIN___, PIN___ },
    { BORDER, BORDER, PIN___, PIN___, PIN___, BORDER, BORDER },
    { BORDER, BORDER, PIN___, PIN___, PIN___, BORDER, BORDER },
};

/* Matrix esperada */
uint8_t matrixEspected[MATRIX_ROW_SIZE][MATRIX_COL_SIZE] = {
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
    { HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__ },
    { HOLE__, HOLE__, HOLE__, PIN___, HOLE__, HOLE__, HOLE__ },
    { HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__ },
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
};

/* Estatísticas de dados */
statistics_t statistics;

/*************************************************************************************
* Private prototypes
*************************************************************************************/
void printMatrix(int row_arg, int col_arg, uint8_t piece[row_arg][col_arg]);
void printMatrixFile(int row_arg, int col_arg, uint8_t piece[row_arg][col_arg], FILE* fp);
void printStatisticsFile(statistics_t* statistics, piece_info_t* pieceInfo, char* filename);
bool checkPosition (int row, int col, int rot);
bool findPosition(int* rot_arg, int* row_arg, int* col_arg);
void rotateMatrix(int row, int col, uint8_t matrix[row][col], int rotations);

/*******************************************************************************
   main
*****************************************************************************//*
 * @brief The main function.
 * @param void
 * @return TRUE Solution found.
           FALSE Otherwise.
*******************************************************************************/
int main()
{
    memset(&statistics, 0, sizeof(statistics));

    piece_info_t pieceInfo[NUMBER_OF_PIECES];
    memset(pieceInfo, 0, sizeof(pieceInfo));

    /* Arquivo para saída de dados */
    FILE* fp = fopen("result.txt","w");

    /* Imprime data/hora atual */
    time_t rawtime;
    struct tm* timeinfo;
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    fprintf(fp, "INICIO: %s\r\n", asctime(timeinfo));

    printf("Inicializando ferramenta de solucao do jogo 'Resta Um'\r\n\r\n");

    const int numberOfPieces = NUMBER_OF_PIECES;
    const int numberOfRemaining = NUMBER_OF_REMAINING;

    /* Para cada peça (exceto a última) */
    for(int pieceCount = 0; pieceCount < numberOfPieces; pieceCount++)
    {        
        /* Verifica se atingiu a quantidade desejada de restantes */
        if(pieceCount == (numberOfPieces - numberOfRemaining))
        {
            if(memcmp(matrix, matrixEspected, sizeof(matrix)))
            {
                for(int i=0; i<numberOfPieces - numberOfRemaining; i++)
                {
                    /* Imprime no arquivo */
                    fprintf(fp, "%02d ------------------\r\n\r\n", i);
                    printMatrixFile(MATRIX_ROW_SIZE, MATRIX_COL_SIZE, pieceInfo[i].matrix, fp);
                }

                /* Imprime data/hora atual */
                time(&rawtime);
                timeinfo = localtime(&rawtime);
                fprintf(fp, "CONTINUING: %s\r\n", asctime(timeinfo));

                /* Retorna posição anterior */
                goto REMOVE_PIN;
            }
            else
                break;
        }

        /* Verifica se hash já foi utilizada */
        uint32_t hash = crc32_fast(matrix, sizeof(matrix), 0);
        for(int i=0; i<pieceInfo[pieceCount].hashesSize; i++)
        {
            /* Caso afirmativo, pula etapa de verificação */
            if(pieceInfo[pieceCount].hashes[i] == hash)
                goto REMOVE_PIN;
            else
                continue;
        }

#ifdef DEBUG_CLOCK
        /* Clocks */
        uint64_t start = rdtsc();
        uint64_t end = rdtsc();
        printf("CLOCK NOT FOUND: %lu\r\n", end - start);
        system("pause");
#endif
        /* Procura uma posição na matrix */
        if(findPosition(&pieceInfo[pieceCount].rot, &pieceInfo[pieceCount].row, &pieceInfo[pieceCount].col))
        {
            /* STATS: Incrementa nova inserção da peça */
            statistics.inserts++;

            int row = pieceInfo[pieceCount].row;
            int col = pieceInfo[pieceCount].col;
            int rot = pieceInfo[pieceCount].rot;

            /* Pino central fica livre */
            matrix[row][col] = HOLE__;

            /* Para cima */
            if(rot == UP)
            {
                matrix[row - 1][col]    = HOLE__;
                matrix[row - 2][col]    = PIN___;
            }
            /* Para direita */
            else if(rot == RIGHT)
            {
                matrix[row][col + 1]    = HOLE__;
                matrix[row][col + 2]    = PIN___;
            }
            /* Para baixo */
            else if(rot == DOWN)
            {
                matrix[row + 1][col]    = HOLE__;
                matrix[row + 2][col]    = PIN___;
            }
            /* Para esquerda */
            else if(rot == LEFT)
            {
                matrix[row][col - 1]    = HOLE__;
                matrix[row][col - 2]    = PIN___;
            }

            /* Guarda matrix atual */
            memcpy(pieceInfo[pieceCount].matrix, matrix, sizeof(matrix));

#ifdef DEBUG_PRINTF
            printf("-- POSICAO ENCONTRADA: piece = %d, rot = %d, row = %d, col = %d\r\n\r\n", pieceCount, pieceInfo[pieceCount].rot, pieceInfo[pieceCount].row, pieceInfo[pieceCount].col);
            printMatrix(MATRIX_ROW_SIZE, MATRIX_COL_SIZE, matrix);
            system("pause");
#endif
            continue;
        }
        else
        {
#ifdef USE_ROTATE_SYMETRY
            /* Utiliza a propriedade de rotação da matrix quadrada */
            for(int rot=0; rot<4; rot++)
#endif
            {
                /* Guarda matrix atual como hash a ser comparada */
                int size = pieceInfo[pieceCount].hashesSize;
                if(!(size % MALLOC_ARRAY_SIZE))
                {
                    /* Realoca a memória para o array */
                    uint32_t* pointer = realloc(pieceInfo[pieceCount].hashes, (size + MALLOC_ARRAY_SIZE)*sizeof(int*));
                    if(!pointer)
                    {
                        printf("LIMITE DE MEMORIA ATINGIDO!\r\n");
                        printf("interactions = %lu\r\n", statistics.interractions);
                        printf("inserts = %lu\r\n", statistics.inserts);
                        printf("removes = %lu\r\n", statistics.removes);
                        printf("memory = %lu\r\n", statistics.memory);
                        printf("\r\n");

                        system("pause");
                        return false;
                    }
                    pieceInfo[pieceCount].hashes = pointer;
                    statistics.memory += MALLOC_ARRAY_SIZE*sizeof(int*);
                }

                /* Guarda hash atual */
                pieceInfo[pieceCount].hashes[size] = crc32_fast(matrix, sizeof(matrix), 0);
                pieceInfo[pieceCount].hashesSize++;

#ifdef USE_ROTATE_SYMETRY
                /* Rotaciona a matrix */
                rotateMatrix(MATRIX_ROW_SIZE, MATRIX_COL_SIZE, matrix, 1);
#endif
            }

            /* Remove a peça da matrix */
            REMOVE_PIN:

            /* STATS: Peça removida da matrix */
            statistics.removes++;

            /* Reset nas configurações da peça atual */
            pieceInfo[pieceCount].rot = 0;
            pieceInfo[pieceCount].row = 0;
            pieceInfo[pieceCount].col = 0;

            /* Retorna para a peça anterior */
            pieceCount--;
            if(pieceCount < 0)
            {
                /* Imprime data/hora atual */
                time(&rawtime);
                timeinfo = localtime(&rawtime);
                fprintf(fp, "FIM: %s\r\n", asctime(timeinfo));
                fclose(fp);

                printf("SOLUCAO NAO ENCONTRADA!\r\n");
                system("pause");
                return false;
            }

            int row = pieceInfo[pieceCount].row;
            int col = pieceInfo[pieceCount].col;
            int rot = pieceInfo[pieceCount].rot;

            /* Pino central fica ocupado */
            matrix[row][col] = PIN___;

            /* Para cima */
            if(rot == UP)
            {
                matrix[row - 1][col]    = PIN___;
                matrix[row - 2][col]    = HOLE__;
            }
            /* Para direita */
            else if(rot == RIGHT)
            {
                matrix[row][col + 1]    = PIN___;
                matrix[row][col + 2]    = HOLE__;
            }
            /* Para baixo */
            else if(rot == DOWN)
            {
                matrix[row + 1][col]    = PIN___;
                matrix[row + 2][col]    = HOLE__;
            }
            /* Para esquerda */
            else if(rot == LEFT)
            {
                matrix[row][col - 1]    = PIN___;
                matrix[row][col - 2]    = HOLE__;
            }

#ifdef DEBUG_PRINTF
            printf("|| POSICAO NAO ENCONTRADA: piece = %d, rot = %d, row = %d, col = %d\r\n\r\n", pieceCount, pieceInfo[pieceCount].rot, pieceInfo[pieceCount].row, pieceInfo[pieceCount].col);
            printMatrix(MATRIX_ROW_SIZE, MATRIX_COL_SIZE, matrix);
            system("pause");
#endif
            /* Compensa o acrescimo dado pelo loop for para a próxima interação */
            pieceCount--;
            continue;
        }
    }

    for(int i=0; i<numberOfPieces - numberOfRemaining; i++)
    {
        /* Imprime no arquivo */
        fprintf(fp, "%02d ------------------\r\n\r\n", i);
        printMatrixFile(MATRIX_ROW_SIZE, MATRIX_COL_SIZE, pieceInfo[i].matrix, fp);
    }

    /* Imprime no arquivo data/hora atual */
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    fprintf(fp, "FIM: %s\r\n", asctime(timeinfo));
    fclose(fp);

    /* imprime estatisticas no arquivo */
    printStatisticsFile(&statistics, pieceInfo, "statistics.txt");

    printf("SOLUCAO ENCONTRADA!\r\n");
    system("pause");
    return true;
}

/*******************************************************************************
   printMatrix
*****************************************************************************//*
 * @brief
 * @param
 * @return void.
*******************************************************************************/
void printMatrix(int row_arg, int col_arg, uint8_t piece[row_arg][col_arg])
{
    for(int row = 0; row < row_arg; row++)
    {
        for(int col = 0; col < col_arg; col++)
        {
            if(piece[row][col] == BORDER)
                printf("   ");
            else if(piece[row][col] == HOLE__)
                printf(" o ");
            else
                printf(" * ");
        }
        printf("\n");
    }
    printf("\n");
}

/*******************************************************************************
   printMatrixFile
*****************************************************************************//*
 * @brief
 * @param
 * @return void.
*******************************************************************************/
void printMatrixFile(int row_arg, int col_arg, uint8_t piece[row_arg][col_arg], FILE* fp)
{
    for(int row = 0; row < row_arg; row++)
    {
        for(int col = 0; col < col_arg; col++)
        {
            if(piece[row][col] == BORDER)
                fprintf(fp, "   ");
            else if(piece[row][col] == HOLE__)
                fprintf(fp, " o ");
            else
                fprintf(fp, " * ");
        }
        fprintf(fp, "\n");
    }
    fprintf(fp, "\n");
}

/*******************************************************************************
   printStatisticsFile
*****************************************************************************//*
 * @brief
 * @param
 * @return void.
*******************************************************************************/
void printStatisticsFile(statistics_t* statistics, piece_info_t* pieceInfo, char* filename)
{
    FILE* fp = fopen(filename,"w");

    /* Estatísticas */
    fprintf(fp, "STATISTICS ------------------\r\n");
    fprintf(fp, "interactions = %lu\r\n", statistics->interractions);
    fprintf(fp, "inserts = %lu\r\n", statistics->inserts);
    fprintf(fp, "removes = %lu\r\n", statistics->removes);
    fprintf(fp, "memory = %lu\r\n", statistics->memory);

    /* Peças */
    for(int i=0; i<NUMBER_OF_PIECES; i++)
    {
        fprintf(fp, "PIECE %02d ------------------\r\n", i);
        fprintf(fp, "row = %d\r\n", pieceInfo[i].row);
        fprintf(fp, "col = %d\r\n", pieceInfo[i].col);
        fprintf(fp, "rot = %d\r\n", pieceInfo[i].rot);
        fprintf(fp, "hashesSize = %d\r\n", pieceInfo[i].hashesSize);
    }

    fclose(fp);
}

/*******************************************************************************
   findPosition
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
bool findPosition(int* rot_arg, int* row_arg, int* col_arg)
{
    /* Cria um identificador único da combinação anterior, com base na rotação, linha e coluna */
    int lastUniqueCombination = ((*rot_arg) << 16) | ((*row_arg) << 8) | (*col_arg);

    /* Para cada possível ROTAÇÃO da peça */
    for(int rot = 0; rot < PIN_ROT_SIZE; rot++)
    {
        /* Para cada LINHA */
        for(int row = 0; row < MATRIX_ROW_SIZE; row++)
        {
            /* Para cada COLUNA */
            for(int col = 0; col < MATRIX_COL_SIZE; col++)
            {
                /* Testa se a combinação é possível */
                if(checkPosition (row, col, rot))
                {
                    /* Verifica se a combinação atual já foi utilizada anteriormente, com base no identificador único */
                    int currentUniqueCombination = (rot << 16) | (row << 8) | col;
                    if(lastUniqueCombination >= currentUniqueCombination)
                        continue;

                    /* Guarda a nova combinação encontrada */
                    *rot_arg = rot;
                    *row_arg = row;
                    *col_arg = col;

                    return true;
                }
            }
        }
    }

    /* Nenhuma posição possível encontrada */
    return false;
}

/*******************************************************************************
   checkPosition
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
bool checkPosition (int row, int col, int rot)
{
    /* STATS: Nova interação */
    statistics.interractions++;

    /* Verifica se é um pino */
    if(matrix[row][col] != PIN___)
        return false;

    /* Para cima */
    if(rot == UP)
    {
        /* Verifica se há espaço para a ação */
        if(row < 2)
            return false;
        /* Verifica se 1 posição ACIMA também é um pino */
        if(matrix[row - 1][col] != PIN___)
            return false;
        /* Verifica se 2 posições ACIMA é um furo */
        if(matrix[row - 2][col] != HOLE__)
            return false;
    }
    /* Para direita */
    else if(rot == RIGHT)
    {
        /* Verifica se há espaço para a ação */
        if(col > (MATRIX_COL_SIZE - 3))
            return false;
        /* Verifica se 1 posição à DIREITA também é um pino */
        if(matrix[row][col + 1] != PIN___)
            return false;
        /* Verifica se 2 posições à DIREITA é um furo */
        if(matrix[row][col + 2] != HOLE__)
            return false;
    }
    /* Para baixo */
    else if(rot == DOWN)
    {
        /* Verifica se há espaço para a ação */
        if(row > (MATRIX_ROW_SIZE - 3))
            return false;
        /* Verifica se 1 posição ABAIXO também é um pino */
        if(matrix[row + 1][col] != PIN___)
            return false;
        /* Verifica se 2 posições ABAIXO é um furo */
        if(matrix[row + 2][col] != HOLE__)
            return false;
    }
    /* Para esquerda */
    else if(rot == LEFT)
    {
        /* Verifica se há espaço para a ação */
        if(col < 2)
            return false;
        /* Verifica se 1 posição à ESQUERDA também é um pino */
        if(matrix[row][col - 1] != PIN___)
            return false;
        /* Verifica se 2 posições à ESQUERDA é um furo */
        if(matrix[row][col - 2] != HOLE__)
            return false;
    }
    else
        return false;

    /* Posição foi encontrada */
    return true;
}

/*******************************************************************************
   cyclic_roll
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
void cyclic_roll(uint8_t* a, uint8_t* b, uint8_t* c, uint8_t* d)
{
   uint8_t temp = *a;
   *a = *b;
   *b = *c;
   *c = *d;
   *d = temp;
}

/*******************************************************************************
   rotateMatrix
*****************************************************************************//*
 * @brief
 * @param
 * @return
*******************************************************************************/
void rotateMatrix(int row, int col, uint8_t matrix[row][col], int rotations)
{
    for(int rot=0; rot<rotations; rot++)
    {
        for(int i=0; i<row/2; i++)
            for(int j=0; j<(col+1)/2; j++)
                cyclic_roll(&matrix[i][j], &matrix[row-1-j][i], &matrix[row-1-i][col-1-j], &matrix[j][col-1-i]);
    }
}
