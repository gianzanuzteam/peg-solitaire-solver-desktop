/*************************************************************************************
* Includes
*************************************************************************************/
#include "pins.h"

/*************************************************************************************
* Private variables
*************************************************************************************/

/* Reference board matrix */
const uint8_t referenceMatrix[MATRIX_ROW_SIZE][MATRIX_COL_SIZE] = {
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
    { HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__ },
    { HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__ },
    { HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__, HOLE__ },
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
    { BORDER, BORDER, HOLE__, HOLE__, HOLE__, BORDER, BORDER },
};
